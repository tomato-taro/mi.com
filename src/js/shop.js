import $ from './jquery.js';
import cookie from './library/cookie.js';

let shop = cookie.get('shop');
shop = JSON.parse(shop);
let idList = shop.map(el => el.id).join();
$.ajax({
    type: "get",
    url: "../interface/shop.php",
    data: { idList },
    dataType: "json",
}).then(res=>{
    let template='';
    res.forEach(el => {
        let pic=JSON.parse(el.picture)
        let currentData = shop.filter(elm => elm.id === el.id);
        template+=` 
        <div class="list-item">
          <div class="item-box">
            <div class="item-row">
              <div class="col col-check">
                <i class="iconfont icon-checkbox">✔</i>
              </div>
              <div class="col col-img">
                <img src="./${pic[1].src}" alt="" />
              </div>
              <div class="col col-name">${el.title}</div>
              <div class="col col-price"><span class='unitPrice'>${
                el.price
              }</span>元
              </div>
              <div class="col col-num">
                <div class="change-goods-num">
                  <a href="javascript:;" class="dec" data-id='${el.id}'>-</a>
                  <input type="text" value="${currentData[0].num}" class='J_count' />
                  <a href="javascript:;" class="add" data-id='${el.id}'>+</a>
                </div>
              </div>
              <div class="col col-total"><span class='total_price'> ${(+el.price * currentData[0].num)}</span>元</div>
              <div class="col col-action">
                <a href="javascript:;" class="rem" data-id='${el.id}'>x</a>
              </div>
            </div>
          </div>
        </div>`
    });
    $('.list-body').html(template);
    // 删除
    $('.rem').on('click',function(){
      let getshop=cookie.get("shop");
      getshop=JSON.parse(getshop);
      let result=getshop.filter((el) => el.id != $(this).attr("data-id"));
      cookie.set("shop", JSON.stringify(result));
      $(this).parent().parent().parent().parent().remove();
    });
    // 减
    $(".dec").on("click", function () {
      if ($(this).next().val() > 1) {
        $(this)
          .next()
          .attr("value", $(this).next().val() - 1);
        shop.forEach((el) => {
          if (el.id === $(this).attr("data-id")) {
            el.num--;
          }
        });
        cookie.set("shop", JSON.stringify(shop));
        let val =
          $(this).next().val() *
          $(this).parent().parent().prev().find(".unitPrice").text();
          $(this).parent().parent().next().find('.total_price').text(val);
      }
    });
    // 加
    $(".add").on("click", function () {
        $(this)
          .prev()
          .attr("value", +$(this).prev().val() + 1);
        shop.forEach((el) => {
          if (el.id === $(this).attr("data-id")) {
            el.num++;
          }
        });
        cookie.set("shop", JSON.stringify(shop));
        let val =
          $(this).prev().val() *
          $(this).parent().parent().prev().find(".unitPrice").text();
          $(this).parent().parent().next().find('.total_price').text(val);
    });

    let flaga = 0;
    $(".J_allCheck").on("click", function () {
      if (flaga === 0) {
        
        $(this).addClass("active");
        $(".col-check").children(".icon-checkbox").addClass("active");
        flaga = 1;
        $(".cart-bar>a").removeClass("btn-disabled");
      } else {
       
        $(this).removeClass("active");
        $(".col-check").children(".icon-checkbox").removeClass("active");
        flaga = 0;
        $(".cart-bar>a").addClass("btn-disabled");
      }
      if ($(".active").length) {
        let val = 0;
        $.each($(".total_price"), function (i, el) {
          val += +$(el).text();
        });
        $(".sum").html(val);
      } else {
        $(".sum").html(0);
      }
    });

    $(".icon-checkbox").on("click", function () {
      let flag = 1;
      let flag1 = 0;
      $(this).toggleClass("active");
      if ($(this).hasClass("active")) {
        let val =
          +$(".sum").text() +
          +$(this).parent().parent().find(".total_price").text();
        $(".sum").html(val);
        $(".total").text(
          +$(this).parent().parent().find(".J_count").val() +
            +$(".total").text()
        );  
        $(".cart-bar>a").removeClass("btn-disabled");
      } else {
        let val =
          $(".sum").text() -
          $(this).parent().parent().find(".total_price").text();
        $(".sum").html(val);
       
        $(".total").text(
          +$(".total").text() -
            $(this).parent().parent().find(".J_count").val()
        );
      }
      
      $(".icon-checkbox").each((i, el) => {
        if (!$(el).hasClass("active")) {
          flag = 0;
        } else {
          flag1 = 1;
        }
      });
      if (flag) {
        $(".J_allCheck").addClass("active");
      } else {
        $(".J_allCheck").removeClass("active");
      }
      if (flag1) {
        $(".cart-bar>a").removeClass("btn-disabled");
      } else {
        $(".cart-bar>a").addClass("btn-disabled");
      }
    });
    $(function(){
      $(".icon-checkbox").on('click',function(){
        $(".btn").toggleClass("btn-disabled");
        if($(".icon-checkbox").hasClass("active")){
          $(".btn").removeClass("btn-disabled");
        }
      });
      $(".J_allCheck").on('click',function(){
        $(".btn").removeClass("btn-disabled");
        if($(this).hasClass("active")){
          $(".btn").removeClass("btn-disabled");
        }
      });
      });
}).catch(xhr => {
    console.log(xhr);
});