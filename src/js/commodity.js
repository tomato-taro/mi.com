import commodityAjax from './commodity_ajax.js';
new commodityAjax().init();

import commodityNav from './commodity_nav.js';
new commodityNav().init();

import commodityCheckbox from './commodity_check.js';
new commodityCheckbox().init();

import homePageTab from './homepage-tab-box.js';
new homePageTab().init();



