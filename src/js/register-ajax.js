import $ from "./jquery.js";
class registerAjax{
  constructor(){}
   
  init(){
     this.setacton();
  }
  setacton(){
    $("button").on("click", function () {
      if ($("#ucheck").is(":checked")) {
        $.ajax({
          url: "../interface/reg.php", 
          type: "get", 
          data: {
            country: $("#country").val(),
            phone: $("#phone").val(),
          }, 
          dataType: "json", 
        }).then((res) => {
            if(res[1].error==1){
                alert('注册失败,手机号已注册');
            }else if(res[1].error==0) 
            {
                alert('注册成功');
                location.href = "./login.html";
            }
          })
          .catch((err) => {
            console.log(err, "error");
          });
      } else {
        alert("请勾选协议");
      }
    });
  }; 
};
export default registerAjax
