import $ from './jquery.js';
import cookie from './library/cookie.js';

class commodityAjax{
    constructor(){} 
    init(){
       this.setacton();
    };
    setacton(){
        let id=location.search.split('=')[1];

$.ajax({
    type: "get",
    url: "../interface/getitem.php",
    data: { id },
    dataType: "json",
}).then(res=>{
    let pic = JSON.parse(res.picture);
    let tp=JSON.parse(res.type);
     let template=` <img src="./${pic[0].src}" alt="" />`;
    $('.img-left').html(template);
    template=`<h2>${res.title}</h2>`;
    $('.text-right>h2').replaceWith(template);
    template=`<p class="desc">${res.detail}元</p>`;
    $('.text-right>.desc').html(template);
    // 选择版本
    template="";
    tp.forEach((el)=>{
        template+=`
        <div class="box-folat change-versions">${el.type}</div>`;
    });
    $(".edition").html(template);
    $(".edition>div").eq(0).addClass("active");
    template = "";
    tp[0].color.forEach((el) => {
        template += ` <div class="box-folat change-hue">${el}</div>`;
      });
      $(".color").html(template);
      $(".color>div").eq(0).addClass("active");
      template=` <div>
      <div>
      ${res.title}
        <span>${res.price}元
          <del>${res.ogprice}</del>
        </span>
      </div>
      <div>
          <div  class="total-price">总计：${res.price}元</div>
         </div>
    </div>`
    $('.j_select-list').html(template);




    template=`  <div class="product-box">
    <div class="nav-bar">
      <div class="container">
        <h2>${res.title}</h2>
        <div class="left">
          <span>|</span>
          <a href="">${res.title}</a>
        </div>
        <div class="right">
          <a href="">概述页</a>
          <span> | </span>
          <a href="">参数页</a>
          <span> | </span>
          <a href="">F码通道</a>
          <span> | </span>
          <a href="">咨询客服</a>
          <span> | </span>
          <a href="">用户评价</a>
        </div>
      </div>
    </div>
  </div>`
    $('.big-product-box').html(template);




    if(res.id!=100005){
        template=`<span class="num1">${tp[0].price[0]}元</span>
        <span class="num2">${tp[0].price[1]}元</span>`
        $('.desc3').html(template);     
    }else{
        template=`<span class="num1">${tp[0].price}元</span>`
        $('.desc3').html(template);
    } 
    $('.change-versions').on('click',function(){
        $('.change-versions').removeClass('active');
        $(this).addClass('active');
    });
    $('.change-hue').on('click',function(){
        $('.change-hue').removeClass('active');
        $(this).addClass('active');
    });

    $('#addItem').on('click', function() {
        addItem(res.id, '1');
    });

   
}).catch(err=>{
    console.log(err)
});
function addItem(id, num) {
    let product = { id, num };

    // console.log(product);

    let shop = cookie.get('shop'); // 获取cookie中的shop


    if (shop) { // 判断shop已有数据
        shop = JSON.parse(shop); // shop 字符串转数组

        // 需要判断当前的商品再shop中是否已存在 如果存在则修改数量 不存在则添加
        if (shop.some(el => el.id === id)) { // 判断当前商品存在
            let index = shop.findIndex(elm => elm.id === id); // 找到商品的索引
            let count = parseInt(shop[index].num);
            count += parseInt(num);
            shop[index].num = count;
        } else {
            shop.push(product);
        }

    } else {
        shop = [];
        shop.push(product);
    }

    cookie.set('shop', JSON.stringify(shop));
};

    };
};
export default commodityAjax;