import $ from "./jquery.js";
class loginStyle{
  constructor(){}
   
  init(){
     this.setacton();
  }
  setacton(){
    //用户名输入框动画
$("#uname").on("blur", function () {
  if ($("#uname").val() && $("#password").val()) {
    $("button").css({ opacity: 1, cursor: "pointer" });
    $("button").removeAttr("disabled");
  } else {
    $("button").css({ opacity: 0.4, cursor: "default" });
    $("button").attr("disabled", "true");
  }
  if ($("#uname").val()) {
    $(".label_name").css({
      "font-size": "12px",
      top: "10px",
    });
    $(".hint_1").css("display", "none");
  } else {
    $(".label_name").css({
      "font-size": "17px",
      top: "20px",
      color: "#f04645",
    });
    $("#uname").css("background-color", "#fcf2f3");
    $(".hint_1").css("display", "block");
  }
});
$("#uname").on("focus", function () {
  $(".label_name").css({
    "font-size": "12px",
    top: "10px",
  });
  $("#uname").on("input", function () {
    if ($("#uname").val()) {
      $(".hint_1").css("display", "none");
      $("#uname").css("background-color", "#fff");
      $(".label_name").css({
        color: "rgba(0, 0, 0, 0.4)",
      });
    } else {
      $(".hint_1").css("display", "block");
    }
  });
});
//密码输入框动画
$("#password").on("blur", function () {
  if ($("#uname").val() && $("#password").val()) {
    $("button").css({ opacity: 1, cursor: "pointer" });
    $("button").removeAttr("disabled");
  } else {
    $("button").css({ opacity: 0.4, cursor: "default" });
    $("button").attr("disabled", "true");
  }
  if ($("#password").val()) {
    $(".label_pwd").css({
      "font-size": "12px",
      top: "10px",
    });
    $(".hint_2").css("display", "none");
  } else {
    $(".label_pwd").css({
      "font-size": "17px",
      top: "20px",
      color: "#f04645",
    });
    $("#password").css("background-color", "#fcf2f3");
    $(".hint_2").css("display", "block");
  }
});
$("#password").on("focus", function () {
  $(".label_pwd").css({
    "font-size": "12px",
    top: "10px",
  });

  $("#password").on("input", function () {
    if ($("#password").val()) {
      $(".hint_2").css("display", "none");
      $("#password").css("background-color", "#fff");
      $(".label_pwd").css({
        color: "rgba(0, 0, 0, 0.4)",
      });
    } else {
      $(".hint_2").css("display", "block");
    }
  });
});

// 国家
$("#country").on("blur", function () {
  if ($("#country").val() && $("#phone").val()) {
    $("button").css({ opacity: 1, cursor: "pointer" });
    $("button").removeAttr("disabled");
  } else {
    $("button").css({ opacity: 0.4, cursor: "default" });
    $("button").attr("disabled", "true");
  }
  if ($("#country").val()) {
    $(".lable-country").css({
      "font-size": "12px",
      top: "10px",
    });
    $(".hint-country").css("display", "none");
  } else {
    $(".lable-country").css({
      "font-size": "17px",
      top: "20px",
      color: "#f04645",
    });
    $("#country").css("background-color", "#fcf2f3");
    $(".hint-country").css("display", "block");
  }
});
$("#country").on("focus", function () {
  $(".lable-country").css({
    "font-size": "12px",
    top: "10px",
  });
  $("#country").on("input", function () {
    if ($("#country").val()) {
      $(".hint-country").css("display", "none");
      $("#country").css("background-color", "#fff");
      $(".lable-country").css({
        color: "rgba(0, 0, 0, 0.4)",
      });
    } else {
      $(".hint-country").css("display", "block");
    }
  });
});


// 手机号
$("#phone").on("blur", function () {
  if ($("#phone").val() && $("#phone").val()) {
    $("button").css({ opacity: 1, cursor: "pointer" });
    $("button").removeAttr("disabled");
  } else {
    $("button").css({ opacity: 0.4, cursor: "default" });
    $("button").attr("disabled", "true");
  }
  if ($("#phone").val()) {
    $(".lable-phone").css({
      "font-size": "12px",
      top: "10px",
    });
    $(".hint-phone").css("display", "none");
  } else {
    $(".lable-phone").css({
      "font-size": "17px",
      top: "20px",
      color: "#f04645",
    });
    $("#phone").css("background-color", "#fcf2f3");
    $(".hint-phone").css("display", "block");
  }
});
$("#phone").on("focus", function () {
  $(".lable-phone").css({
    "font-size": "12px",
    top: "10px",
  });
  $("#phone").on("input", function () {
    if ($("#phone").val()) {
      $(".hint-phone").css("display", "none");
      $("#phone").css("background-color", "#fff");
      $(".lable-phone").css({
        color: "rgba(0, 0, 0, 0.4)",
      });
    } else {
      $(".hint-phone").css("display", "block");
    }
  });
});



// 验证码
$("#numeric").on("blur", function () {
  if ($("#numeric").val() && $("#numeric").val()) {
    $("button").css({ opacity: 1, cursor: "pointer" });
    $("button").removeAttr("disabled");
  } else {
    $("button").css({ opacity: 0.4, cursor: "default" });
    $("button").attr("disabled", "true");
  }
  if ($("#numeric").val()) {
    $(".lable-numeric").css({
      "font-size": "12px",
      top: "10px",
    });
    $(".hint-numeric").css("display", "none");
  } else {
    $(".lable-numeric").css({
      "font-size": "17px",
      top: "20px",
      color: "#f04645",
    });
    $("#numeric").css("background-color", "#fcf2f3");
    $(".hint-numeric").css("display", "block");
  }
});
$("#numeric").on("focus", function () {
  $(".lable-numeric").css({
    "font-size": "12px",
    top: "10px",
  });
  $("#numeric").on("input", function () {
    if ($("#numeric").val()) {
      $(".hint-numeric").css("display", "none");
      $("#numeric").css("background-color", "#fff");
      $(".lable-numeric").css({
        color: "rgba(0, 0, 0, 0.4)",
      });
    } else {
      $(".hint-numeric").css("display", "block");
    }
  });
});
  };
};
export default loginStyle