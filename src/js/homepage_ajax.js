import $ from './jquery.js';

class homePageAjax{
    constructor(){}
     
    init(){
       this.setacton();
    }
    setacton(){
        $.ajax({
            type: "get",
            url: "../interface/getitems.php",
            dataType: "json",
          }).then(res=>{
            let template='';
            res.forEach(el=>{
               let pic=JSON.parse(el.picture);
              template+=`
                <li class="brick-iteam">
                  <a href="commodity.html?id=${el.id}">
                    <div class="figure">
                      <img src="./${pic[1].src}" alt="${pic[1].alt}" />
                    </div>
                    <h3 class="title">${el.title}</h3>
                    <p class="desc">${el.abstract}</p>
                    <p class="price">
                      <span>${el.price}</span>
                      <span class="num">${el.ogprice}</span>
                    </p>
                  </a>
                </li>`
            });
            $('.brick-list').eq(0).html(template)
        
          }).catch(err=>{
        console.log(err)
          });
        
    }
}
export default  homePageAjax