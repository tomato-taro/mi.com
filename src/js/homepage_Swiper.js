import {Swiper} from './library/swiper-7.4.1/swiper/swiper-bundle.esm.browser.js';

class homePageSwiper{
    constructor(){}
     
    init(){
       this.setacton();
    }
    setacton(){
        var mySwiper = new Swiper('.swiper', {
            effect: 'fade',
            loop: true, 
            pagination: {
              el: '.swiper-pagination',
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
              autoplay: {
                delay: 3000,
                disableOnInteraction: false,
                },
          });
    }
};
export default homePageSwiper